# Base Image
FROM ubuntu:16.04

MAINTAINER Scott Christley <scott.christley@utsouthwestern.edu>

# PROXY: uncomment these if building behind UTSW proxy
#ENV http_proxy 'http://proxy.swmed.edu:3128/'
#ENV https_proxy 'https://proxy.swmed.edu:3128/'
#ENV HTTP_PROXY 'http://proxy.swmed.edu:3128/'
#ENV HTTPS_PROXY 'https://proxy.swmed.edu:3128/'

# Install OS Dependencies
RUN DEBIAN_FRONTEND='noninteractive' apt-get update && DEBIAN_FRONTEND='noninteractive' apt-get install -y \
    make \
    wget \
    xz-utils \
    default-jre \
    git

##################
##################

# not currently using redis and supervisor

# old stuff
#    nodejs \
#    nodejs-legacy \
#    npm \
#    redis-server \
#    redis-tools \
#    sendmail-bin \
#    supervisor \

# Setup postfix
# The postfix install won't respect noninteractivity unless this config is set beforehand.
#RUN mkdir /etc/postfix
#RUN touch /etc/mailname
#COPY docker/postfix/main.cf /etc/postfix/main.cf
#COPY docker/scripts/postfix-config-replace.sh /root/postfix-config-replace.sh

# Debian vociferously complains if you try to install postfix and sendmail at the same time.
#RUN DEBIAN_FRONTEND='noninteractive' apt-get install -y -q --force-yes \
#    postfix

##################
##################

# node
RUN wget https://nodejs.org/dist/v8.10.0/node-v8.10.0-linux-x64.tar.xz
RUN tar xf node-v8.10.0-linux-x64.tar.xz
RUN cp -rf /node-v8.10.0-linux-x64/bin/* /usr/local/bin
RUN cp -rf /node-v8.10.0-linux-x64/lib/* /usr/local/lib
RUN cp -rf /node-v8.10.0-linux-x64/include/* /usr/local/include
RUN cp -rf /node-v8.10.0-linux-x64/share/* /usr/local/share

RUN npm install -g swagger

RUN mkdir /api-js-mongodb
RUN mkdir /api-js-mongodb/app

# PROXY: More UTSW proxy settings
#RUN npm config set proxy http://proxy.swmed.edu:3128
#RUN npm config set https-proxy http://proxy.swmed.edu:3128

# Install npm dependencies (optimized for cache)
COPY app/package.json /api-js-mongodb/app
RUN cd /api-js-mongodb/app && npm install

# pull in sway bug fix for array parameters
RUN cd /api-js-mongodb/app && npm install https://github.com/apigee-127/sway.git#94ba34f --save

# Setup redis
#COPY docker/redis/redis.conf /etc/redis/redis.conf

# Setup supervisor
#COPY docker/supervisor/supervisor.conf /etc/supervisor/conf.d/

# Copy project source
COPY . /api-js-mongodb

# Copy AIRR spec
RUN cp /api-js-mongodb/airr-standards/specs/common_repository_api.yaml /api-js-mongodb/app/api/swagger/common_repository_api.yaml
RUN cp /api-js-mongodb/airr-standards/specs/airr-schema.yaml /api-js-mongodb/app/config/airr-schema.yaml

CMD ["node", "--harmony", "/api-js-mongodb/app/app.js"]
