'use strict';

var app = require('express')();
var path = require('path');
var fs = require('fs');
var yaml = require('js-yaml');
var Runner = require('swagger-node-runner');

// Server environment config
var config = require('./config/config');
var airr = require('./api/helpers/airr-schema');

module.exports = app; // for testing

// Swagger middleware config
var swaggerConfig = {
  appRoot: __dirname, // required config
  configDir: 'config'
};

// Load swagger API
//console.log(config.appRoot);
var swaggerFile = path.resolve(swaggerConfig.appRoot, 'api/swagger/common_repository_api.yaml');
console.log('Using swapper API file: ' + swaggerFile);
swaggerConfig.swagger = yaml.safeLoad(fs.readFileSync(swaggerFile, 'utf8'));

airr.schema().then(function(schema) {
    // store the schema as a global so all code can see it
    console.log('Loaded AIRR schema.');
    global.airr = schema;

    Runner.create(swaggerConfig, function(err, runner) {
	if (err) { throw err; }

	// install middleware
	var swaggerExpress = runner.expressMiddleware();
	swaggerExpress.register(app);

	var port = config.port || 8080;
	app.listen(port);

	console.log('AIRR Common Repository API listening on port:' + port);
    });
})
.catch(function(err) {
    console.error('Failed to load AIRR schema.');
    console.error(err);
})
